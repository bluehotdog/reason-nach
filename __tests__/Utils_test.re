open Jest;

describe("Utils", () =>
  Utils.(
    Expect.(
      describe("Utils", () => {
        test("pad correctly to the left", () =>
          expect(
            addPadding("123", ~padChar='a', ~totalLength=6, ~direction=Left),
          )
          |> toEqual("aaa123")
        );
        test("pad correctly to the right", () =>
          expect(
            addPadding("123", ~padChar='a', ~totalLength=6, ~direction=Right),
          )
          |> toEqual("123aaa")
        );
      })
    )
  )
);