open Jest;

describe("Validation", () =>
  Validation.(
    Expect.(
      describe("Alphanumeric", () => {
        test("returns true for valid alphanumeric", () => {
          let validNumbers = [|"1234", "0123", "1120", "a123", "asdf"|];
          let res = Array.map(alphanumeric, validNumbers);
          expect(res) |> not_ |> toContain(false);
        });
        test("returns false for invalid alphanumeric", () => {
          let validNumbers = [|"a12.34", "-123", "1120$"|];
          let res = Array.map(alphanumeric, validNumbers);
          expect(res) |> not_ |> toContain(true);
        });
      })
    )
  )
);