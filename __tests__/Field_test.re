open Jest;
[%%debugger.chrome];
open Expect;
open Rationale.Result;
open Belt.Result;
describe("Field", () =>
  Field.(
    /* TODO: BS-Jest needs to add support for test.each to cleanup the repetition here */
    describe("encode", () => {
      test("RecordTypeCode", () =>
        RecordTypeCode("6")
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("6"))
      );

      test("TransactionCode", () =>
        TransactionCode(22)
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("22"))
      );
      test("ReceivingDFI", () =>
        ReceivingDFI("00001234")
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("00001234"))
      );
      test("CheckDigit", () =>
        CheckDigit(1) |> validate <$> encode |> expect |> toEqual(Ok("1"))
      );
      test("DFIAccount", () =>
        DFIAccount("1234")
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("1234             "))
      );
      test("Amount", () =>
        Amount(123)
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("00000123cc"))
      );
      test("IDNumber", () =>
        IDNumber("123")
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("123            "))
      );
      test("IndividualName", () => {
        let a = IndividualName("Some Name") |> validate;
        a <$> encode |> expect |> toEqual(Ok("Some Name             "));
      });
      test("DiscretionaryData", () =>
        DiscretionaryData("xx")
        |> validate
        <$> encode
        |> expect
        |> toEqual(Ok("xx"))
      );
      test("TraceNumber", () =>
        TraceNumber(2) |> validate <$> encode |> expect |> toEqual(Ok("2"))
      );
    })
  )
);