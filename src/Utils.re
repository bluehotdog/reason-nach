type direction =
  | Left
  | Right;

let addPadding = (str, ~padChar: char, ~totalLength, ~direction) => {
  let len = totalLength - String.length(str);
  let padStr = String.make(len, padChar);
  switch (direction) {
  | Left => padStr ++ str
  | Right => str ++ padStr
  };
};