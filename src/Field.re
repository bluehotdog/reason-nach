open Belt;

type fieldType =
  | Alphanumeric(string)
  | Numeric(int)
  | Amount(int);

type field =
  | RecordTypeCode(string)
  | TransactionCode(int)
  | ReceivingDFI(string)
  | CheckDigit(int)
  | DFIAccount(string)
  | Amount(int)
  | IDNumber(string)
  | IndividualName(string)
  | DiscretionaryData(string)
  | AddendaId(int)
  | TraceNumber(int);

type validField =
  | ValidField(field);

let value = (field: field) =>
  switch (field) {
  | RecordTypeCode(val_) => Alphanumeric(val_)
  | TransactionCode(val_) => Numeric(val_)
  | ReceivingDFI(val_) => Alphanumeric(val_)
  | CheckDigit(val_) => Numeric(val_)
  | DFIAccount(val_) => Alphanumeric(val_)
  | Amount(val_) => Amount(val_)
  | IDNumber(val_) => Alphanumeric(val_)
  | IndividualName(val_) => Alphanumeric(val_)
  | DiscretionaryData(val_) => Alphanumeric(val_)
  | AddendaId(val_) => Numeric(val_)
  | TraceNumber(val_) => Numeric(val_)
  };

let asString = (field: field) => {
  let valType = value(field);
  switch (valType) {
  | Amount(i)
  | Numeric(i) => string_of_int(i)
  | Alphanumeric(str) => str
  };
};

let name = (field: field) =>
  switch (field) {
  | RecordTypeCode(_) => "Record Type Code"
  | TransactionCode(_) => "Transaction Code"
  | ReceivingDFI(_) => "Receiving DFI Identification"
  | CheckDigit(_) => "Check Digit"
  | DFIAccount(_) => "DFI Account Number"
  | Amount(_) => "Amount"
  | IDNumber(_) => "Individual Identification Number"
  | IndividualName(_) => "Individual Name"
  | DiscretionaryData(_) => "Discretionary Data"
  | AddendaId(_) => "Addenda Record Indicator"
  | TraceNumber(_) => "Trace Number"
  };

let width = (field: field): int =>
  switch (field) {
  | RecordTypeCode(_) => 1
  | TransactionCode(_) => 2
  | ReceivingDFI(_) => 8
  | CheckDigit(_) => 1
  | DFIAccount(_) => 17
  | Amount(_) => 10 /* should be 10, but we're adding cc implicitly as we keep the type int */
  | IDNumber(_) => 15
  | IndividualName(_) => 22
  | DiscretionaryData(_) => 2
  | AddendaId(_) => 15
  | TraceNumber(_) => 1
  };

let position = (field: field): int =>
  switch (field) {
  | RecordTypeCode(_) => 1
  | TransactionCode(_) => 2
  | ReceivingDFI(_) => 4
  | CheckDigit(_) => 12
  | DFIAccount(_) => 13
  | Amount(_) => 30
  | IDNumber(_) => 40
  | IndividualName(_) => 55
  | DiscretionaryData(_) => 77
  | AddendaId(_) => 79
  | TraceNumber(_) => 80
  };

type validationErr =
  | IncorrectLength(int, int)
  | NotEqualError(string)
  | CantBeEmpty
  | MandatoryFieldError
  | IncorrectData(fieldType);

let encode = (validField: validField): string => {
  let strValue = fieldValue =>
    switch (fieldValue) {
    | Alphanumeric(str) => str
    | Amount(int) => string_of_int(int) ++ "cc"
    | Numeric(int) => string_of_int(int)
    };
  let direction = fieldValue =>
    switch (fieldValue) {
    | Numeric(_) => Utils.Left
    | Alphanumeric(_) => Utils.Right
    | Amount(_) => Utils.Left
    };
  let padChar = fieldValueType =>
    switch (fieldValueType) {
    | Numeric(_) => '0'
    | Alphanumeric(_) => ' '
    | Amount(_) => '0'
    };

  let getInnerField = validField =>
    switch (validField) {
    | ValidField(field) => field
    };

  let innerField = getInnerField(validField);
  let fieldValue = innerField |> value;
  let val_ = innerField |> value |> strValue;
  let width = innerField |> width;

  Utils.addPadding(
    val_,
    ~padChar=padChar(fieldValue),
    ~totalLength=width,
    ~direction=direction(fieldValue),
  );
};

let validate = (field: field): Result.t(validField, validationErr) => {
  open Rationale.Result;
  let checkLength = field => {
    let isStrict = field =>
      switch (field) {
      | RecordTypeCode(_)
      | TransactionCode(_)
      | ReceivingDFI(_)
      | CheckDigit(_)
      | AddendaId(_) => true
      | TraceNumber(_)
      | DFIAccount(_)
      | Amount(_)
      | IDNumber(_)
      | IndividualName(_)
      | DiscretionaryData(_) => false
      };

    let width = width(field);
    let length = asString(field) |> String.length;
    let checkStrict = isStrict(field);
    length == width || length < width && !checkStrict ?
      Result.Ok(field) : Result.Error(IncorrectLength(width, length));
  };
  let checkType = (field: field) => {
    let val_ = value(field);
    let validate = val_ =>
      switch (val_) {
      | Alphanumeric(string) => Validation.alphanumeric(string)
      | Amount(int) => int >= 0
      | Numeric(_) => true
      };
    let res = val_ |> validate;
    res ? Result.Ok(field) : Result.Error(IncorrectData(val_));
  };

  let setValid = (field: field): Result.t(validField, 'a) =>
    Result.Ok(ValidField(field));

  Ok(field) >>= checkLength >>= checkType >>= setValid;
};