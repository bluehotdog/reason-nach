open Belt;
type t = {
  recordTypeCode: Field.field,
  transactionCode: Field.field,
  receivingDFI: Field.field,
  checkDigit: Field.field,
  dfiAccount: Field.field,
  amount: Field.field,
  idNumber: Field.field,
  individualName: Field.field,
  discretionaryData: Field.field,
  addendaId: Field.field,
  traceNumber: Field.field,
};

let defaultRecordTypeCode = "6";
let create =
    (
      ~transactionCode,
      ~receivingDFI,
      ~checkDigit,
      ~dfiAccount,
      ~amount,
      ~idNumber="",
      ~individualName="",
      ~traceNumber,
      (),
    )
    : t => {
  recordTypeCode: Field.RecordTypeCode(defaultRecordTypeCode),
  transactionCode: Field.TransactionCode(transactionCode),
  receivingDFI: Field.ReceivingDFI(receivingDFI),
  checkDigit: Field.CheckDigit(checkDigit),
  dfiAccount: Field.DFIAccount(dfiAccount),
  amount: Field.Amount(amount),
  idNumber: Field.IDNumber(idNumber),
  individualName: Field.IndividualName(individualName),
  discretionaryData: Field.DiscretionaryData(""),
  addendaId: Field.AddendaId(0),
  traceNumber: Field.TraceNumber(traceNumber),
};

type error =
  | InvalidValue(string, string)
  | InvalidTransactionCode(int);
let calculateCheckDigit = (routingNumber: string): int => {
  let n = int_of_char(routingNumber.[0]);
  let sum = ref(n * 3);
  let n = int_of_char(routingNumber.[1]);
  sum := sum^ + n * 7;
  let n = int_of_char(routingNumber.[2]);
  sum := sum^ + n;
  let n = int_of_char(routingNumber.[3]);
  sum := sum^ + n * 3;
  let n = int_of_char(routingNumber.[4]);
  sum := sum^ + n * 7;
  let n = int_of_char(routingNumber.[5]);
  sum := sum^ + n;
  let n = int_of_char(routingNumber.[6]);
  sum := sum^ + n * 3;
  let n = int_of_char(routingNumber.[7]);
  sum := sum^ + n * 7;

  Js.Math.ceil(float_of_int(sum^) /. 10.0) * 10 - sum^;
};
let validate = (entry: t) => {
  let validateField = (field: Field.field) =>
    switch (field) {
    | RecordTypeCode(string_) =>
      string_ == defaultRecordTypeCode ?
        Result.Ok() : Error(InvalidValue(string_, defaultRecordTypeCode))
    | TransactionCode(val_) =>
      Validation.isTransactionCode(val_) ?
        Result.Ok() : Error(InvalidTransactionCode(val_))
    | CheckDigit(_) => Result.Ok() /* TODO: check against calculated check digit*/
    | ReceivingDFI(_)
    | DFIAccount(_)
    | Amount(_)
    | IDNumber(_)
    | IndividualName(_)
    | DiscretionaryData(_)
    | AddendaId(_)
    | TraceNumber(_) => Result.Ok()
    };
  let fields = [|
    entry.recordTypeCode,
    entry.transactionCode,
    entry.receivingDFI,
    entry.checkDigit,
    entry.dfiAccount,
    entry.amount,
    entry.idNumber,
    entry.individualName,
    entry.discretionaryData,
    entry.addendaId,
    entry.traceNumber,
  |];
  Array.reduce(fields, Result.Ok(), (_result, field) =>
    validateField(field)
  );
};